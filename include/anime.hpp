#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sstream>
#include <algorithm>
#include <regex>
#include "json.hpp"
#include "utils.hpp"
#include "config.hpp"

using json = nlohmann::json;

#ifndef   ANIME_HPP
#define   ANIME_HPP

class Anime
{
private:
    int index;
    json data;
    std::string path;
    std::string folder;
    std::string configFilePath;
    std::string currentFilename;

    std::string getNextEpisodeFilename(int episode);
public:
    Anime(int index, json data, std::string configFilePath);
    Anime();
    ~Anime();

    std::string audioId;
    int currentEpisode;
    std::string currentTime;
    std::string subId;
    std::string name;
    int lastEpisode;

    void nextEpisode();
    void save();
    std::string getFullAnimeFilenamePath();
};

Anime::Anime(int index, json data, std::string configFilePath)
{
    this->index = index;
    this->data = data;
    this->configFilePath = configFilePath;

    std::string rawMasterPath = data["master-path"].dump();
    std::string rawAudio = data["animes"][index]["audio-id"].dump();
    std::string rawEpisode = data["animes"][index]["current-episode"].dump();
    std::string rawTime = data["animes"][index]["current-time"].dump();
    std::string rawSub = data["animes"][index]["sub-id"].dump();
    std::string rawFolder = data["animes"][index]["folder"].dump();
    std::string rawEpisodeFile = data["animes"][index]["episode-file"].dump();
    std::string rawName = data["animes"][index]["name"].dump();
    std::string rawLastEpisode = data["animes"][index]["last-episode"].dump();

    audioId = Utils::removeChar(rawAudio, '"');
    currentEpisode = std::stoi(Utils::removeChar(rawEpisode, '"'));
    lastEpisode = std::stoi(Utils::removeChar(rawLastEpisode, '"'));
    currentTime = Utils::removeChar(rawTime, '"');
    subId = Utils::removeChar(rawSub, '"');
    currentFilename = Utils::removeChar(rawEpisodeFile, '"');
    path = Utils::removeChar(rawMasterPath, '"');
    folder = Utils::removeChar(rawFolder, '"');
    name = Utils::removeChar(rawName, '"');
}

Anime::Anime()
{
}

Anime::~Anime()
{
}

void Anime::nextEpisode()
{
    currentTime = "0.0";
    currentEpisode++;
    currentFilename = getNextEpisodeFilename(currentEpisode);
}

void Anime::save()
{
    data["animes"][index]["audio-id"] = audioId;
    data["animes"][index]["current-episode"] = currentEpisode;
    data["animes"][index]["current-time"] = currentTime;
    data["animes"][index]["sub-id"] = subId;
    data["animes"][index]["episode-file"] = currentFilename;

    Config::setConfig(data,configFilePath);
}

std::string Anime::getFullAnimeFilenamePath()
{
    return std::string("\"") + path + folder + currentFilename + std::string("\"");
}

std::string Anime::getNextEpisodeFilename(int episode)
{
    std::string command = std::string("/bin/ls ") + path + folder + " | head -"+ std::to_string(episode) + " | tail +" + std::to_string(episode); 

    return Utils::removeChar(Utils::executeCommand(command),'\n');
}

#endif