#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <algorithm>
#include "include/json.hpp"
#include "include/anime.hpp"
#include "include/videoplayer.hpp"
#include "include/utils.hpp"
#include "include/config.hpp"

using json = nlohmann::json;

#define DEFAULT_CONFIG_FILE_PATH "/.config/cranime.config"

int main(int argc, char** argv)
{
    json data;
    
    int animeIndex = -1;
    std::string homePath;
    std::string configFilePath;

    std::string command = std::string("cd ~;pwd"); 
    FILE *proc = popen(command.c_str(),"r");
    char buf[1024];
    while ( !feof(proc) && fgets(buf,sizeof(buf),proc) )
    {
        homePath = Utils::removeChar(std::string(buf),'\n');
        break;
    }

    configFilePath = homePath + std::string(DEFAULT_CONFIG_FILE_PATH);
    std::ifstream configFile(configFilePath);
    if(!configFile.is_open())
    {
        Config::generateConfigFile(homePath, configFilePath);
        configFile.open(configFilePath);
    }
    configFile >> data;
    configFile.close();

    if(argc > 1)
    {
        if(argv[1] == std::string("-l") || argv[1] == std::string("--list"))
        {
            std::cout << "\n";
            if(data["animes"].size() != 0)
            {
                for(auto anime : data["animes"])
                {
                    std::cout << Utils::removeChar(anime["name"].dump(), '"') << " (" << anime["current-episode"] << "/" << anime["last-episode"] << ")" << std::endl;
                }
            }
            else
                std::cout << "List is currently empty!" << std::endl;
            std::cout << "\n";
        }
        else if(argv[1] == std::string("--help") || argv[1] == std::string("-h"))
        {
            Utils::getHelpOutput();
        }
        else if(argv[1] == std::string("--generate-config"))
        {
            Config::generateConfigFile(homePath, configFilePath);
        }
        else if(argv[1] == std::string("--master-path"))
        {
            if(argc > 2)
                Config::setMasterPath(std::string(argv[2]), configFilePath);
            else
                std::cout << "There is no argument for path\nI.e.: cranime --master-path /home/user/videos/animes/";
        }
        else if(argv[1] == std::string("--add") || argv[1] == std::string("-a"))
        {
            if(argc > 3)
                Config::addAnime(std::string(argv[2]),std::string(argv[3]), configFilePath);
            else
                std::cout << "There is not enough argumens to add an anime\nI.e.: cranime -a LuckyStar/ LuckyStar";
        }
        else if(argv[1] == std::string("--remove") || argv[1] == std::string("-r"))
        {
            if(argc > 1)
            {
                animeIndex = Utils::findIndex(data,argv[2]);
                if(animeIndex == -1)
                    std::cout << "Anime not found!" << std::endl;
                else
                    Config::removeAnime(data, animeIndex, configFilePath);
            }
            else
                std::cout << "Anime not found!" << std::endl;
        }
        else if(argv[1] == std::string("--start") || argv[1] == std::string("-s"))
        {
            int index = 0;
            if(argc > 2)
            {
                animeIndex = Utils::findIndex(data,argv[2]);
                if(animeIndex == -1)
                    std::cout << "Anime not found!" << std::endl;
                else
                {
                    Anime currAnime = Anime(animeIndex, data, configFilePath);
                    Videoplayer mp =  Videoplayer(currAnime, data);
                    mp.start();
                }
            }
            else
                std::cout << "There is no argument for anime\nI.e.: cranime --start Gantz";
        }
    }
    else
        Utils::getHelpOutput();

    return 0;
}