#include <mpv/client.h>
#include <vector>
#include <string>
#include "anime.hpp"
#include "json.hpp"
using json = nlohmann::json;

#ifndef   MPV_H
#define   MPV_H

class Videoplayer
{
public:
    Videoplayer(Anime anime, json animeReg);
    Videoplayer(Videoplayer &&) = default;
    Videoplayer(const Videoplayer &) = default;
    Videoplayer &operator=(Videoplayer &&) = default;
    Videoplayer &operator=(const Videoplayer &) = default;
    ~Videoplayer();

    void start();

private:
    Anime anime;
    json animeReg;

    static inline void checkError(int status);
};

Videoplayer::Videoplayer(Anime anime, json animeReg)
{
    this->anime = Anime(anime);
    this->animeReg = animeReg;
}

Videoplayer::~Videoplayer()
{
}

inline void Videoplayer::checkError(int status)
{
    if (status < 0) {
        printf("Videoplayer API error: %s\n", mpv_error_string(status));
        exit(1);
    }
}

void Videoplayer::start()
{
    bool closed = false;
    mpv_handle *ctx = mpv_create();
    if (!ctx) { printf("failed creating context\n"); }

    checkError(mpv_set_option_string(ctx, "input-default-bindings", "yes"));
    mpv_set_option_string(ctx, "input-vo-keyboard", "yes");
    int val = 1;
    checkError(mpv_set_option(ctx, "osc", MPV_FORMAT_FLAG, &val));
    checkError(mpv_initialize(ctx));
    mpv_set_property_string(ctx, "start", anime.currentTime.c_str());

    while(!closed && anime.currentEpisode <= anime.lastEpisode)
    {
        std::string command = "loadfile " + anime.getFullAnimeFilenamePath();
        checkError(mpv_command_string(ctx, command.c_str()));
        std::cout << "Playing " << anime.name << " (" << anime.currentEpisode << "/" << anime.lastEpisode << ")" << std::endl;
        mpv_set_property_string(ctx, "sub", anime.subId.c_str());
        mpv_set_property_string(ctx, "audio", anime.audioId.c_str());

        while (1)
        {
            mpv_event *event = mpv_wait_event(ctx, 10000);
            if(event->event_id == MPV_EVENT_TRACK_SWITCHED)
            {
                if(mpv_get_property_string(ctx, "current-tracks/sub") != NULL)
                    anime.subId = mpv_get_property_string(ctx, "current-tracks/sub/id");
                if(mpv_get_property_string(ctx, "current-tracks/audio") != NULL)
                    anime.audioId = mpv_get_property_string(ctx, "current-tracks/audio/id");
                anime.save();
            }
            if(event->event_id == MPV_EVENT_PAUSE || event->event_id == MPV_EVENT_SEEK)
            {
                double backupTime = atof(mpv_get_property_string(ctx, "time-pos"));
                backupTime = backupTime - 5.0 >= 0 ? backupTime - 5.0 : 0.0;
                anime.currentTime = std::to_string(backupTime);
                anime.save();
            }
            if (event->event_id == MPV_EVENT_SHUTDOWN)
            {
                anime.save();
                closed = true;
                break;
            }
            if(event->event_id == MPV_EVENT_IDLE)
            {
                anime.nextEpisode();
                anime.save();
                break;
            }
        }
    }
    mpv_terminate_destroy(ctx);
    if(anime.currentEpisode > anime.lastEpisode)
        std::cout << "Anime reached the end! :)" << std::endl;
}

#endif