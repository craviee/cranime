#include <string>
#include "json.hpp"
#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "utils.hpp"

using json = nlohmann::json;

#ifndef   CONFIG_HPP
#define   CONFIG_HPP

class Config
{
public:
    static void generateConfigFile(std::string homePath, std::string configFilePath);
    static std::string getCurrentMasterPath(std::string configFilePath);
    static void setMasterPath(std::string newMasterPath, std::string configFilePath);
    static void setConfig(json data, std::string configFilePath);
    static json getConfig(std::string configFilePath);
    static void addAnime(std::string animePath, std::string animeName,std::string configFilePath);
    static void removeAnime(json data, int index, std::string configFilePath);
};

void Config::generateConfigFile(std::string homePath, std::string configFilePath)
{
    json data;
    std::string defaultMasterPath = homePath + std::string("/videos/animes/");
    data["master-path"] = defaultMasterPath;
    std::vector<std::string> vector;
    data["animes"] = vector;
    setConfig(data, configFilePath);
    std::cout << "Config file generated at " << configFilePath <<
        "\nThe current anime folder is " << Config::getCurrentMasterPath(configFilePath) <<
        "\nChange the anime folder with:\ncranime --master-path [PATH]\n" <<
        "I.e.: cranime --master-path /home/user/videos/animes/" << std::endl;
}

std::string Config::getCurrentMasterPath(std::string configFilePath)
{
    json data = getConfig(configFilePath);
    return Utils::removeChar(data["master-path"].dump(), '"');
}


void Config::setMasterPath(std::string newMasterPath, std::string configFilePath)
{
    json data = getConfig(configFilePath);
    data["master-path"] = newMasterPath;
    setConfig(data,configFilePath);
}

void Config::setConfig(json data, std::string configFilePath)
{
    std::ofstream o(configFilePath);
    o << std::setw(4) << data << std::endl;
    o.close();
}

json Config::getConfig(std::string configFilePath)
{
    json data;
    std::ifstream configFile(configFilePath);
    if(!configFile.is_open())
    {
        std::cout << "Cannot open config file at " << configFilePath <<
        "!\nThis file exists?" << std::endl;
    }
    configFile >> data;
    configFile.close();
    return data;
}

void Config::addAnime(std::string animePath, std::string animeName,std::string configFilePath)
{
    json data = getConfig(configFilePath);
    std::string masterPath = getCurrentMasterPath(configFilePath);
    std::string animeFullPath = masterPath + animePath;
    int firstEpisode = 1;
    int lastEpisode = std::stoi(Utils::executeCommand(std::string("/bin/ls ") + animeFullPath + std::string(" | wc -l")));
    std::string firstEpisodeFile = Utils::executeCommand(std::string("/bin/ls ") + animeFullPath + std::string(" | head -")
     + std::to_string(firstEpisode) + std::string(" | tail +") + std::to_string(firstEpisode)); 
    int index = data["animes"].size();

    data["animes"][index]["audio-id"] = "1";
    data["animes"][index]["current-episode"] = firstEpisode;
    data["animes"][index]["current-time"] = "0.0";
    data["animes"][index]["episode-file"] = firstEpisodeFile;
    data["animes"][index]["folder"] =  animePath;
    data["animes"][index]["last-episode"] = lastEpisode;
    data["animes"][index]["name"] = animeName;
    data["animes"][index]["sub-id"] = "1";

    setConfig(data,configFilePath);

    std::cout << "Animed added!" << std::endl;
}

void Config::removeAnime(json data, int index, std::string configFilePath)
{
    data["animes"].erase(index);
    setConfig(data, configFilePath);
    std::cout << "Animed removed!" << std::endl;
}

#endif