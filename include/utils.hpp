#include <string>
#include "json.hpp"
#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>

using json = nlohmann::json;

#ifndef   UTILS_HPP
#define   UTILS_HPP

class Utils
{
public:
    static std::string removeChar(std::string str, char c);
    static void getHelpOutput();
    static std::string executeCommand(std::string command);
    static int findIndex(json data, std::string animeName);
    
};

std::string Utils::removeChar(std::string str, char c)
{
    str.erase(std::remove(str.begin(), str.end(), c), str.end());
    return str;
}

void Utils::getHelpOutput()
{
    std::cout << ("\nCranime v1.0\n")
    << ("Usage: cranime [OPTION] [ARGUMENT]\n\n")
    << ("Options:\n\n")
        << ("--help / -h\t\t\t prints this menu\n")
        << ("--generate-config\t\t generates config file at ~/.config/cranime.config\n")
        << ("--master-path [PATH]\t\t updates the path of your anime's folder I.e.: cranime --master-path /home/user/videos/animes/\n")
        << ("--add / -a [FOLDER] [NAME]\t adds an anime to your list, the folder must be relative to your master-path I.e.: cranime -a LuckyStar/ LuckyStar\n")
        << ("--list / -l\t\t\t lists the current animes in your list\n" )
        << ("--start / -s [NAME]\t\t starts the anime defined in the list\n")
        << ("--remove / -r [NAME]\t\t removes the anime defined in the list\n") << std::endl;
        
}

std::string Utils::executeCommand(std::string command)
{
    FILE *proc = popen(command.c_str(),"r");
    char buf[1024];
    std::string returnString;
    while ( !feof(proc) && fgets(buf,sizeof(buf),proc) )
    {
        returnString = std::string(buf);
        returnString.erase(std::remove(returnString.begin(), returnString.end(), '\n'), returnString.end());
        break;
    }
    return returnString;
}

int Utils::findIndex(json data, std::string animeName)
{
    int currentIndex = 0;
    int animeIndex = -1;
    for(auto anime : data["animes"])
    {
        if(std::string(anime["name"]) == animeName)
        {
            animeIndex = currentIndex;
            break;
        }
        currentIndex++;
    }
    return animeIndex;
}

#endif