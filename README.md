# cranime
Keep your tracks of anime using mpv!

## Features

- Track the episode and time you paused to continue later!
- Save the configuration for audio and subtitles!
- The next episode will open after the previous one finished!

## Dependencies

- mpv

## Instalation

1. Download the repo and enter the folder
2. Compile with `make`
3. The program `cranime` is generated

## Usage

`./cranime` will generate the config file in `~/.config/cranime.config`
You can modify the path of you anime's folder using `--master-path` command or modifying manually in the config file
Use `./cranime --help` for more information

## Thanks

- [nlohmann](https://github.com/nlohmann/json) for providing [json.hpp](https://github.com/nlohmann/json/blob/develop/include/nlohmann/json.hpp) file

